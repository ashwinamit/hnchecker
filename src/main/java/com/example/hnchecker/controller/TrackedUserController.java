package com.example.hnchecker.controller;

import com.example.hnchecker.entity.TrackedUser;
import com.example.hnchecker.service.TrackedUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ashwin on 10/1/2015.
 */
@RestController()
@RequestMapping(value = "users")
public class TrackedUserController {

    private final TrackedUserService userService;

    @Autowired
    public TrackedUserController(TrackedUserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/")
    private List<TrackedUser> getReplies() {
        return userService.findAll();
    }

}
