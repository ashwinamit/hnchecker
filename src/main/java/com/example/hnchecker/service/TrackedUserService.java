package com.example.hnchecker.service;

import com.example.hnchecker.dao.TrackedUserRepository;
import com.example.hnchecker.entity.TrackedUser;
import com.gs.collections.impl.list.mutable.FastList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Created by ashwin on 10/3/2015.
 */
@Service
public class TrackedUserService {

    private final TrackedUserRepository userRepo;

    @Autowired
    public TrackedUserService(TrackedUserRepository userRepo) {
        this.userRepo = userRepo;
    }

    public Set<String> getTrackedUsernames() {
        return FastList.newList(userRepo.findAll()).collect(TrackedUser::getHnUsername).toSet();
    }

    public TrackedUser save(TrackedUser user) {
        return userRepo.save(user);
    }

    public List<TrackedUser> findAll() {
        return FastList.newList(userRepo.findAll());
    }
}
