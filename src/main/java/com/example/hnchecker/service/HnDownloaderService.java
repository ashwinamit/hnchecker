package com.example.hnchecker.service;

import com.example.hnchecker.entity.HnSearchResponse;
import com.example.hnchecker.entity.Item;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.JdkFutureAdapters;
import com.google.common.util.concurrent.ListenableFuture;
import com.gs.collections.impl.block.factory.Predicates;
import com.gs.collections.impl.list.mutable.FastList;
import com.gs.collections.impl.list.mutable.ListAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.client.AsyncRestTemplate;

import java.time.Instant;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by ashwin on 10/1/2015.
 */
@Service
public class HnDownloaderService {

    private static final Logger logger = LoggerFactory.getLogger(HnDownloaderService.class);

    /**
     * Only download comments since it's not possible for a story to be a reply to someone elses comment
     * <p>
     * lower bound (unix epoch in seconds)
     * upper bound (unix epoch, seconds)
     * page (int)
     * page size (int)
     */
    private static final String DOWNLOAD_RECENT_ITEMS_TEMPLATE =
            "http://hn.algolia.com/api/v1/search_by_date?tags=comment&numericFilters=created_at_i>%d,created_at_i<%d&page=%d&hitsPerPage=%d";

    /**
     * Download all the comments on a story (also include story to get top level comment)
     * story_id (int)
     * page size (int)
     */
    private static final String DOWNLOAD_STORY_TEMPLATE = "http://hn.algolia.com/api/v1/search?tags=(comment,story),story_%d&hitsPerPage=%d";

    private static final int PAGE_SIZE = 999; //api seems to limit to 1k so go 999 to not trigger errors

    private final AsyncRestTemplate asyncRest;

    @Autowired
    public HnDownloaderService(AsyncRestTemplate asyncRest) {
        this.asyncRest = asyncRest;
    }

    /**
     * Download all comments from ($since to now)
     *
     * @param since time to pull comments from
     * @return all comments since $since
     */
    public List<Item> getItemsSince(Instant since) {
        logger.info("Downloading stories at " + LocalTime.now().toString());
        long now = System.currentTimeMillis() / 1000;
        long sinceSeconds = since.toEpochMilli() / 1000;
        return getStoriesForSearch(sinceSeconds, now);
    }

    private List<Item> getStoriesForSearch(long start, long end) {
        if (end < start) {
            throw new IllegalStateException("end greater then start");
        }
        HnSearchResponse response = getRecentItemsSync(start, end, 0, PAGE_SIZE);
        List<Item> hits = response.getHits();
        logger.info("Request has " + response.getNbHits() + " stories over " + response.getNbPages() + " pages");
        for (int i = 1; i < response.getNbPages(); i++) {
            hits.addAll(getRecentItemsSync(start, end, i, PAGE_SIZE).getHits());
        }
        return hits;
    }

    /**
     * Downloads all the stories comments are from
     *
     * @param items list of comments
     * @return list of stories those comments are from (if 2 comments are on same story, will only return 1 copy of story)
     */
    public List<HnSearchResponse> getStoriesForItems(List<Item> items) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("downloadStories");
        List<HnSearchResponse> stories = downloadStories(ListAdapter.adapt(items).collect(Item::getStoryId).toSet());
        stopWatch.stop();
        logger.debug(stopWatch.prettyPrint());
        return stories;
    }

    private HnSearchResponse getRecentItemsSync(long start, long end, long pageNum, long pageSize) {
        try {
            ResponseEntity<HnSearchResponse> response = getRecentItems(start, end, pageNum, pageSize).get();
            HnSearchResponse search = processHnSearchEntity(response);
            if (search != null)
                return search;
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Lookup failed", e);
            throw new RuntimeException(e);
        }
        throw new IllegalStateException("doSearch returned null entry, check response from service");
    }

    private List<HnSearchResponse> downloadStories(Set<Integer> storyIds) {
        List<ListenableFuture<ResponseEntity<HnSearchResponse>>> responses =
                FastList.newList(storyIds).collect(id -> JdkFutureAdapters.listenInPoolThread(downloadStory(id)));
        ListenableFuture<List<ResponseEntity<HnSearchResponse>>> result = Futures.successfulAsList(responses);
        try {
            return ListAdapter.adapt(result.get()).collect(this::processHnSearchEntity).reject(Predicates.isNull());
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Failed to download stories..." + FastList.newList(storyIds).makeString(), e);
            throw new IllegalStateException("Download stories fail", e);
        }
    }

    private HnSearchResponse processHnSearchEntity(ResponseEntity<HnSearchResponse> search) {
        if (search != null && search.getStatusCode().equals(HttpStatus.OK)) {
            logger.debug("Got good search result with " + search.getBody().getNbHits() + " over " + search.getBody().getNbPages() + " pages");
            return search.getBody();
        } else {
            logger.error("Search got bad response " + (search == null ? "null" : search.getStatusCode()));
        }
        return null;
    }

    private Future<ResponseEntity<HnSearchResponse>> downloadStory(int id) {
        String url = String.format(DOWNLOAD_STORY_TEMPLATE, id, PAGE_SIZE);
        logger.info("Downloading story " + url);
        return asyncRest.getForEntity(url, HnSearchResponse.class);
    }

    private Future<ResponseEntity<HnSearchResponse>> getRecentItems(long start, long end, long pageNum, long pageSize) {
        String url = String.format(DOWNLOAD_RECENT_ITEMS_TEMPLATE, start, end, pageNum, pageSize);
        logger.info("Downloading recent items @ " + url);
        return asyncRest.getForEntity(url, HnSearchResponse.class);
    }


}
