package com.example.hnchecker.service;

import com.example.hnchecker.entity.HnSearchResponse;
import com.example.hnchecker.entity.HnStory;
import com.example.hnchecker.entity.Item;
import com.gs.collections.impl.block.factory.Predicates;
import com.gs.collections.impl.list.mutable.ListAdapter;
import com.gs.collections.impl.map.mutable.MapAdapter;
import com.gs.collections.impl.set.mutable.SetAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Created by ashwin on 10/3/2015.
 */
@Service
public class ReplyNotificationService {

    private static final Logger logger = LoggerFactory.getLogger(ReplyNotificationService.class);


    private final HnDownloaderService hnDownloaderService;
    private final TrackedUserService trackedUserService;

    @Autowired
    public ReplyNotificationService(HnDownloaderService hnDownloaderService, TrackedUserService trackedUserService) {
        this.hnDownloaderService = hnDownloaderService;
        this.trackedUserService = trackedUserService;
    }

    /**
     * 1. Download the comments for last 5 mintes
     * 2. Get the full stories those comments belong to
     * 2a. Build flat stories into threads
     * 3. Get the list of usernames we're tracking from the database
     * 4. Per thread
     * 4a. Check if any posts by tracking usernames
     * 4b. If there are any posts, check for any replies since 5 minutes
     * 4c. Build map of username => reply comments
     * <p>
     * Set up to run on cron - cron schedule should match the $since time as well
     */
    @Scheduled(cron = "0 */5 * * * *")
    public void scanForReplies() {
        StopWatch stopWatch = new StopWatch();
        Instant since = Instant.now().minus(5, ChronoUnit.MINUTES);
        List<Item> items = hnDownloaderService.getItemsSince(since); //1
        List<HnSearchResponse> flatThreads = hnDownloaderService.getStoriesForItems(items); //2
        stopWatch.start("Build thread list");
        List<HnStory> hnStories = ListAdapter.adapt(flatThreads).collect(HnSearchResponse::getHits) //2a
                .reject(hits -> hits.size() == 0)
                .collect(HnStory::buildThread)
                .reject(Predicates.isNull()); // reject any hnStories that failed to build
        stopWatch.stop();
        logger.debug(stopWatch.prettyPrint());

        Set<String> users = trackedUserService.getTrackedUsernames(); //3
        logger.debug("Searching for replies from: " + SetAdapter.adapt(users).makeString());
        stopWatch.start("Check for replies");
        Map<String, List<Item>> userReplies = new HashMap<>();
        hnStories.stream().filter(thread -> thread.hasPostsBy(users)).forEach(thread -> { //4a
            for (String user : users) { //4b
                userReplies.computeIfAbsent(user, e -> new ArrayList<>()) //4c
                        .addAll(thread.repliesForAuthorInTime(user, Date.from(since), true));
            }
        });
        stopWatch.stop();
        logger.debug(stopWatch.prettyPrint());
        logger.debug("Replies: " + MapAdapter.adapt(userReplies).makeString());
    }
}
