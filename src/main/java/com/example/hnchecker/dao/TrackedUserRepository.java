package com.example.hnchecker.dao;

import com.example.hnchecker.entity.TrackedUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by ashwin on 10/3/2015.
 */
@RepositoryRestResource(collectionResourceRel = "tracked", path = "tracked")
public interface TrackedUserRepository extends CrudRepository<TrackedUser, Long> {

}
