package com.example.hnchecker;

import com.example.hnchecker.service.HnDownloaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.AsyncRestTemplate;

@SpringBootApplication
@EnableScheduling
public class HncheckerApplication {

    @Autowired
    private HnDownloaderService downloaderService;

    public static void main(String[] args) {
        SpringApplication.run(HncheckerApplication.class, args);
    }

    @Bean
    AsyncRestTemplate asyncRestTemplate() {
        return new AsyncRestTemplate();
    }


}
