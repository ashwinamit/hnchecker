package com.example.hnchecker.entity;

import com.gs.collections.api.set.MutableSet;
import com.gs.collections.impl.factory.Sets;
import com.gs.collections.impl.map.mutable.MapAdapter;
import com.gs.collections.impl.map.mutable.UnifiedMap;
import com.gs.collections.impl.set.mutable.SetAdapter;
import com.gs.collections.impl.tuple.Tuples;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Encapsulate a thread ressabled from flat list of comments
 * Should make it easy to answer 2 questions
 * 1) Did anyone post a comment directly in reply to one of my comments
 * 2) Did anyone post a comment in tree of replies to my comment (may be arbitrarily deep)
 * <p>
 * <p>
 * This is comment thread
 * Lets say comment thread looks like
 * <p>
 * head
 * - comment 1
 * - comment 3
 * - comment 4
 * - comment 5
 * - comment 2
 * <p>
 * response from api looks like
 * (head, parent: null)
 * (comment1, parent: head)
 * (comment3, parent: comment1)
 * (comment4, parent: comment1)
 * (comment5, parent: comment4)
 * (comment2, parent: head)
 * <p>
 * I rebuild it into tree
 * ..........head
 * ........../...\
 * ...comment1...comment2
 * ....../..........\
 * ..comment4.....comment3
 * ../
 * comment5
 * <p>
 * <p>
 * But ideally I'd have it in
 * (head => [comment1,comment2,...,comment5])
 * (comment1 => [comment3, comment4, comment5])
 * (comment3 => [])
 * (comment4 => [comment5])
 * (comment5 => [])
 * (comment2 => [])
 * Created by ashwin on 10/2/2015.
 */
public class HnStory {

    private static final Logger logger = LoggerFactory.getLogger(HnStory.class);

    private final Map<String, Set<Integer>> authorPosts;
    private final Map<String, Set<Item>> flatPosts;

    /**
     * Turns a flat list of (post, author) into a flatPosts Map of (author) => (set of all comments in to that author)
     * <p>
     * We build tree (thread) but throw it away since we don't really need it
     *
     * @param items flat list of items response from Algoia API
     */
    private HnStory(List<Item> items) {
        if (items.size() == 0) {
            throw new IllegalStateException("Cannot build thread out of 0 items");
        }
        logger.debug("Building thread from " + items.size() + " items");
        authorPosts = new HashMap<>();
        Map<Integer, Item> itemMap = new HashMap<>();
        Map<Integer, List<Item>> idThread = new HashMap<>();
        Item head = null;
        for (Item i : items) {
            authorPosts.computeIfAbsent(i.getAuthor(), s -> new HashSet<>()).add(i.getItemId());
            itemMap.put(i.getItemId(), i);
            idThread.computeIfAbsent(i.getParentId(), s -> new ArrayList<>()).add(i);
            if (i.isStory())
                head = i;
        }
        if (head == null) {
            throw new IllegalStateException("items list is missing story head");
        }
        Map<Item, List<Item>> thread = MapAdapter.adapt(idThread).collect((id, comments) -> Tuples.pair(itemMap.get(id), comments));
        ThreadPost threadTree = ThreadPost.walkAdjacencyMap(head, thread);
        Map<Item, Set<Item>> flattenedThread = buildFlatPosts(threadTree, UnifiedMap.newMap());
        flatPosts = new HashMap<>();
        for (Map.Entry<Item, Set<Item>> v : flattenedThread.entrySet()) {
            flatPosts.computeIfAbsent(v.getKey().getAuthor(), s -> new HashSet<>()).addAll(v.getValue());
        }
    }

    public static HnStory buildThread(List<Item> items) {
        try {
            return new HnStory(items);
        } catch (IllegalStateException e) {
            logger.error("Building thread failed", e);
            return null;
        }
    }

    private static Map<Item, Set<Item>> buildFlatPosts(ThreadPost post, Map<Item, Set<Item>> flatMap) {
        for (ThreadPost child : post.children) {
            buildFlatPosts(child, flatMap);
        }

        if (post.parent != null) {
            Set<Item> items = flatMap.getOrDefault(post.parent.item, new HashSet<>());
            items.add(post.item);
            items.addAll(flatMap.getOrDefault(post.item, new HashSet<>())); //children added themselves here so don't need to use item.children
            flatMap.put(post.parent.item, items);
        }
        return flatMap;
    }

    public boolean hasPostsBy(Set<String> monitoring) {
        return !Sets.intersect(authorPosts.keySet(), monitoring).isEmpty();
    }

    public Set<Item> repliesForAuthorInTime(String authorName, Date since, boolean nested) {
        //use getOrDefault since it's possible that flatPosts(author) is null if there are no replies
        MutableSet<Item> replies = SetAdapter.adapt(flatPosts.getOrDefault(authorName, new HashSet<>()))
                .select(item -> item.getCreatedAt().after(since));
        if (!nested) {
            replies = replies.select(item -> authorPosts.get(authorName).contains(item.getParentId()));
        }
        return replies;
    }

    private static class ThreadPost {
        private final Item item;
        private final List<ThreadPost> children;
        private final ThreadPost parent;

        public ThreadPost(Item item, ThreadPost parent) {
            this.item = item;
            children = new ArrayList<>();
            this.parent = parent;
        }

        public static ThreadPost walkAdjacencyMap(Item headItem, Map<Item, List<Item>> items) {
            ThreadPost head = new ThreadPost(headItem, null);
            return walk(head, items);
        }

        private static ThreadPost walk(ThreadPost head, Map<Item, List<Item>> items) {
            //noinspection StatementWithEmptyBody
            for (Item i : items.getOrDefault(head.item, new ArrayList<>())) {
                ThreadPost iPost = new ThreadPost(i, head);
                walk(iPost, items);
                head.children.add(iPost);
            }
            return head;
        }
    }
}
