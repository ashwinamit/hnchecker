package com.example.hnchecker.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Encapsulate response from Algoia search api
 *
 * Created by ashwin on 10/1/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HnSearchResponse {

    private List<Item> hits;
    private int page;
    private int nbPages;
    private int nbHits;

    public List<Item> getHits() {
        return hits;
    }

    public int getPage() {
        return page;
    }

    public int getNbPages() {
        return nbPages;
    }

    public int getNbHits() {
        return nbHits;
    }

    @Override
    public String toString() {
        return "HnSearchResponse{" +
                "hits=" + hits +
                ", page=" + page +
                ", nbPages=" + nbPages +
                ", nbHits=" + nbHits +
                '}';
    }
}
