package com.example.hnchecker.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by ashwin on 10/3/2015.
 */
@Entity
public class TrackedUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String hnUsername;
    private String emailTo;

    public TrackedUser() { //noargs bean constructor
    }

    public TrackedUser(String hnUsername, String emailTo) {
        this.hnUsername = hnUsername;
        this.emailTo = emailTo;
    }

    public String getHnUsername() {
        return hnUsername;
    }

    public String getEmailTo() {
        return emailTo;
    }
}
