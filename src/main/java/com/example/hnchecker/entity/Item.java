package com.example.hnchecker.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Encapsulate single hits entry from Algoia search API response
 * <p>
 * Created by ashwin on 10/1/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    @JsonProperty("objectID")
    private int itemId;
    @JsonProperty("story_id")
    private int storyId;
    @JsonProperty("parent_id")
    private int parentId;
    private String author;
    @JsonProperty("created_at")
    private Date createdAt;
    @JsonProperty("_tags")
    private Set<String> tags;

    public Item() {
        itemId = -1;
        storyId = -1;
        parentId = -1;
        createdAt = new Date(0);
        author = null;
        tags = new HashSet<>();
    }

    public Item(int itemId, int storyId, int parentId, String author, Date createdAt, Set<String> tags) {
        this.itemId = itemId;
        this.storyId = storyId;
        this.parentId = parentId;
        this.author = author;
        this.createdAt = createdAt;
        this.tags = tags;
    }

    public int getItemId() {
        return itemId;
    }

    public int getStoryId() {
        return storyId;
    }

    public int getParentId() {
        return parentId;
    }

    public boolean isStory() {
        return tags.contains("story");
    }

    public String getAuthor() {
        return author;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemId=" + itemId +
                ", storyId=" + storyId +
                ", parentId=" + parentId +
                ", author='" + author + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (itemId != item.itemId) return false;
        if (storyId != item.storyId) return false;
        if (parentId != item.parentId) return false;
        return author.equals(item.author) && createdAt.equals(item.createdAt);

    }

    @Override
    public int hashCode() {
        int result = itemId;
        result = 31 * result + storyId;
        result = 31 * result + parentId;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        return result;
    }
}
