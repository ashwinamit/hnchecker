package com.example.hnchecker;

import com.example.hnchecker.entity.HnStory;
import com.example.hnchecker.entity.Item;
import com.gs.collections.api.list.MutableList;
import com.gs.collections.impl.list.mutable.FastList;
import com.gs.collections.impl.set.mutable.UnifiedSet;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by ashwin on 10/4/2015.
 */

public class HnStoryUnitTests {

    @Test
    public void testFlatStory() {
        Item head = new Item(1, -1, -1, "headAuthor", new Date(), UnifiedSet.newSetWith("story"));
        Item comment1 = new Item(2, 0, 1, "comment1Author", new Date(), new HashSet<>());
        Item comment2 = new Item(3, 0, 1, "comment2Author", new Date(), new HashSet<>());

        HnStory story = HnStory.buildThread(Arrays.asList(head, comment1, comment2));
        assertNotNull(story);

        MutableList<String> authors = FastList.newListWith("headAuthor", "comment1Author", "comment2Author");
        assertTrue("Finds author", authors.allSatisfy(s -> story.hasPostsBy(UnifiedSet.newSetWith(s))));

        Date from = Date.from(Instant.now().minus(5, ChronoUnit.MINUTES));
        Set<Item> storyReplies = story.repliesForAuthorInTime("headAuthor", from, true);
        assertEquals(UnifiedSet.newSetWith(comment1, comment2), storyReplies);


        Set<Item> commentReply = story.repliesForAuthorInTime("comment1Author", from, true);
        assertEquals(new HashSet<Item>(), commentReply);
    }

    @Test
    public void testTime() {
        Date postTime = Date.from(Instant.now().minus(10, ChronoUnit.MINUTES));
        Date checkPostsSince = Date.from(Instant.now().minus(10, ChronoUnit.MINUTES));

        Item head = new Item(1, -1, -1, "headAuthor", postTime, UnifiedSet.newSetWith("story"));
        Item comment1 = new Item(2, 0, 1, "comment1Author", postTime, new HashSet<>());
        HnStory story = HnStory.buildThread(Arrays.asList(head, comment1));
        assertNotNull(story);
        Set<Item> storyRepliesInTime = story.repliesForAuthorInTime("headAuthor", checkPostsSince, true);
        assertEquals(new HashSet<Item>(), storyRepliesInTime);
    }

    /**
     * Test that we nestedReply flag works
     */
    @Test
    public void testNested() {
        Item head = new Item(1, -1, -1, "headAuthor", new Date(), UnifiedSet.newSetWith("story"));
        Item comment1 = new Item(2, 0, 1, "comment1Author", new Date(), new HashSet<>());
        Item comment1Reply = new Item(3, 0, 2, "comment2Author", new Date(), new HashSet<>());
        HnStory story = HnStory.buildThread(Arrays.asList(head, comment1, comment1Reply));
        assertNotNull(story);

        Date from = Date.from(Instant.now().minus(5, ChronoUnit.MINUTES));
        Set<Item> nestedReplies = story.repliesForAuthorInTime("headAuthor", from, true);
        assertEquals("nestedReplyTest", UnifiedSet.newSetWith(comment1, comment1Reply), nestedReplies);

        Set<Item> flatReplies = story.repliesForAuthorInTime("headAuthor", from, false);
        assertEquals("flatReplyTest", UnifiedSet.newSetWith(comment1), flatReplies);
    }

    /**
     * Had a bug where if thread looked like (assume only comment<b>3</b>Author post in time range)
     * -author
     * -comment1Author
     * --comment2Author
     * ---comment3Author (in range)
     * -comment4Author
     * --comment2Author
     * ---comment5Author
     * <p>
     * So there is 2 replies to comment2Author
     * <p>
     * Only people who should have replies detected are: author, comment1Author, comment2Author), NOT comment<b>4</b>Author
     * <p>
     * But was we were merging posts by author names, so comment3Author got merged under comment2Author, which then got
     * merged under comment4Author (bug wouldn't exist if comment3Author hadn't been processed)
     * <p>
     * Leaving this test to make sure don't regress on this
     */
    @Test
    public void testSameAuthor() {
        Date inRange = new Date();
        Date oldPost = Date.from(Instant.now().minus(10, ChronoUnit.MINUTES));
        Date since = Date.from(Instant.now().minus(5, ChronoUnit.MINUTES));

        Item head = new Item(1, -1, -1, "headAuthor", oldPost, UnifiedSet.newSetWith("story"));
        Item comment1 = new Item(2, 0, 1, "comment1Author", oldPost, new HashSet<>());
        Item comment2 = new Item(3, 0, 2, "comment2Author", oldPost, new HashSet<>());
        Item comment3 = new Item(4, 0, 3, "comment3Author", inRange, new HashSet<>());
        Item comment4 = new Item(5, 0, 1, "comment4Author", oldPost, new HashSet<>());
        Item comment5 = new Item(6, 0, 5, "comment2Author", oldPost, new HashSet<>()); //again by comment2Author
        Item comment6 = new Item(7, 0, 6, "comment5Author", oldPost, new HashSet<>()); //inRange

        HnStory story = HnStory.buildThread(Arrays.asList(head, comment1, comment2, comment3, comment4, comment5, comment6));
        assertNotNull(story);

        FastList.newListWith("comment3Author", "comment4Author", "comment5Author")
                .each(s -> assertEquals(s, new HashSet<>(), story.repliesForAuthorInTime(s, since, true)));


        FastList.newListWith("headAuthor", "comment1Author", "comment2Author")
                .each(s -> assertEquals(s, UnifiedSet.newSetWith(comment3), story.repliesForAuthorInTime(s, since, true)));

    }
}