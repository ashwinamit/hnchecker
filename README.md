# Hn Notifier

*Java Spring App - Scans HN Algoia API for replies*

## Useage

- $ git clone $project
- $ cd $project
- $ mvn spring-boot:run (alternatively mvn package && java -jar target/hnchecker-0.0.1-SNAPSHOT.jar
- $ add some username to track (curl -XPOST localhost:50105/tracked -d '{"hnUsername":"$username", "emailTo":$sendTo"}' -H "Content-Type: application/json")
- $ app automatically scans every 5 minutes
